const { gql } = require('apollo-server');

const typeDefs = gql`
    type Query {
        machines: [Machine!]
    }

    type Machine {
        name: String!
        lastKnownPosition: String
        sensors: [Sensor!]
    }
    
    type Sensor {
        name: String!
        machine: Machine!
    }
`;

module.exports = typeDefs;