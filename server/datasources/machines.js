const { DataSource } = require("apollo-datasource");

class MachinesAPI extends DataSource {
    constructor({ store }) {
        super();
        this.store = store
    }

    async getmachines() {
        const machines = this.store.machines;
        return machines
    }
}

module.exports = MachinesAPI;