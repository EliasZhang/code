module.exports = {
    Query: {
        machines: (_, __, { dataSources }) =>
            dataSources.userAPI.getmachines()
    }
};