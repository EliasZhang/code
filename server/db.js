const db = [
    {
        name: "a",
        lastKnownPosition: "USA",
        sensors: [
            {
                name: "a-1",
                machine: {
                    name: "a-2",
                    lastKnownPosition: "USA",
                }
            }
        ]
    },
    {
        name: "b",
        lastKnownPosition: "Germany",
        sensors: [
            {
                name: "b-1",
                machine: {
                    name: "b-2",
                    lastKnownPosition: "Germany",
                }
            }
        ]
    },
    {
        name: "c",
        lastKnownPosition: "Poland",
        sensors: [
            {
                name: "c-1",
                machine: {
                    name: "c-2",
                    lastKnownPosition: "Poland",
                }
            }
        ]
    }
];

module.exports = db;